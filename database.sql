-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 16-10-2019 a las 19:36:07
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `api_rest_cheil`
--
CREATE DATABASE IF NOT EXISTS `api_rest_cheil` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `api_rest_cheil`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Estelar', '2019-10-16 08:25:45', '2019-10-16 08:25:45'),
(3, 'Basico', '2019-10-16 08:31:04', '2019-10-16 08:31:04'),
(5, 'test', '2019-10-16 16:16:24', '2019-10-16 16:16:24'),
(6, 'Demo', '2019-10-16 16:19:52', '2019-10-16 16:19:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotels`
--

DROP TABLE IF EXISTS `hotels`;
CREATE TABLE IF NOT EXISTS `hotels` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `category_id` int(255) NOT NULL,
  `hotel` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `stars` int(10) NOT NULL,
  `address` varchar(450) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_post_user` (`user_id`),
  KEY `fk_post_category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hotels`
--

INSERT INTO `hotels` (`id`, `user_id`, `category_id`, `hotel`, `content`, `image`, `stars`, `address`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'Nueva Luna', 'El hotel nueva luna es muy bueno', NULL, 4, 'Direccion nueva luna', '2019-10-16 08:41:44', '2019-10-16 19:11:23'),
(2, 1, 2, 'Hotel hilton', 'El hotel Hilton demo es muy regular', NULL, 5, 'Direccion Hilton demo', '2019-10-16 08:44:11', '2019-10-16 08:44:11'),
(3, 1, 2, 'Hotel demo', 'El hotel nueva demoes muy bueno', NULL, 5, 'Direccion nueva demo', '2019-10-16 17:26:31', '2019-10-16 17:26:31'),
(4, 2, 6, 'demo demo', '<p>asdasd asd a</p>', NULL, 5, 'dir demo', '2019-10-16 17:36:16', '2019-10-16 17:36:16'),
(5, 2, 5, 'demo 33', '<p><u>asd asdasda</u></p>', NULL, 5, 'adas d', '2019-10-16 18:21:32', '2019-10-16 18:21:32'),
(6, 2, 2, '555', '<p>555</p>', NULL, 2, '55', '2019-10-16 18:37:52', '2019-10-16 18:37:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `surname` varchar(100) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `role`, `email`, `password`, `description`, `image`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'Juan', 'Carlos', 'user', 'jcfd78@gmail.com', '123', NULL, NULL, NULL, NULL, NULL),
(2, 'Juanito', 'Lopez', 'user', 'juan@lopez.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '<p>editado adsdsa dasdasda</p>', NULL, '2019-10-16 08:05:39', '2019-10-16 16:50:35', NULL),
(3, 'Pepe', 'Perez', 'user', 'pepe@perez.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', NULL, NULL, '2019-10-16 12:09:02', '2019-10-16 12:09:02', NULL),
(4, 'David', 'Lopez', 'user', 'david@david.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', NULL, NULL, '2019-10-16 12:16:25', '2019-10-16 12:16:25', NULL);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `hotels`
--
ALTER TABLE `hotels`
  ADD CONSTRAINT `fk_post_category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `fk_post_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
