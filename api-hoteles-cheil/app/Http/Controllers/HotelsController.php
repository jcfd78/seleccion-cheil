<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Hotels;
use App\User;
use App\Cities;

class HotelsController extends Controller
{
    public function index(Request $request){
    	$hotels = Hotels::all()->load('Category')
                                ->load('User');

    	return response()->json([
			'code' => 200,
			'status' => 'success',
    		'hotels' => $hotels
    	]);
    }

    public function show($id){
    	$hotels = Hotels::find($id)->load('Category')
                                    ->load('User');

    	if (is_object($hotels)) {
    		$data = [
				'code' => 200,
				'status' => 'success',
	    		'hotels' => $hotels
	    	];
    	} else {
    		$data = [
				'code' => 404,
				'status' => 'error',
	    		'message' => 'No se encontró el hotel'
	    	];
    	}

    	return response()->json($data, $data['code']);
    }

    public function store(Request $request){
    	// Recoger los datos por post
    	$json = $request->input('json', null);
    	$params_array = json_decode($json, true);

    	// Validar los datos
    	$validate = \Validator::make($params_array,[
    		'hotel' => 'required',
    		'stars' => 'required|numeric',
    		'address' => 'required',
    		'content' => 'required',
    		'category_id' => 'required',
    		'user_id' => 'required'
    	]);
       	// Guardar la categoria
    	if ($validate->fails()) {
    		$data = [
				'code'    => 404,
				'status'  => 'error',
				'message' => 'No se ha guardado el hotel',
				'error' => $validate->errors() 
	    	];
    	} else {
			$hotels              = new Hotels();
			$hotels->hotel       = $params_array['hotel'];
			$hotels->stars       = $params_array['stars'];
			$hotels->address     = $params_array['address'];
			$hotels->content     = $params_array['content'];
			$hotels->category_id = $params_array['category_id'];
			$hotels->user_id     = $params_array['user_id'];
			$hotels->save();
    		
   			$data = array(
				'code'   => 200,
				'status' => 'success',
				'hotels'   => $hotels
			);
    	}
    	// devolver respuesta
    	return response()->json($data, $data['code']);
    }

    public function update($id, Request $request){
    	$json = $request->input('json', null);
    	$params_array = json_decode($json, true);

    	if (!empty($params_array)) {
    		// Validar los datos
	    	$validate = \Validator::make($params_array,[
	    		'hotel' => 'required|alpha',
	    		'stars' => 'required|numeric',
	    		'address' => 'required',
	    		'content' => 'required',
	    		'category_id' => 'required',
	    	]);
	    	unset($params_array['id']);
	    	unset($params_array['created_at']);
	    	unset($params_array['user_id']);

	    	$hotels_update = Hotels::where('id', $id)->update($params_array);
			$data = array(
				'code'   => 200,
				'status' => 'success',
				'hotels'   => $params_array
			);

    	} else {
    		$data = [
				'code'    => 404,
				'status'  => 'error',
				'message' => 'No has enviado ningún hotel.'
	    	];
    	}
    	// devolver respuesta
    	return response()->json($data, $data['code']);
    }

    public function destroy($id, Request $request){
    	$json = $request->input('json', null);
    	$params_array = json_decode($json, true);

    	$hotels = Hotels::find($id);
        if (is_object($hotels) && !empty($hotels)) {
        	$hotels->delete();
			$data = array(
				'code'   => 200,
				'status' => 'success',
				'message'   => 'El hotel ha sido eliminada'
			);
        } else {
    		$data = [
				'code'    => 404,
				'status'  => 'error',
				'message' => 'No has enviado ningun hotel.'
	    	];
        }

    	// devolver respuesta
    	return response()->json($data, $data['code']);
    }

    public function upload(Request $request){
        // recoger los datos de la peticion
        $image = $request->file('file0');
        // Validamos la imagen recibida
        $validate = \Validator::make($request->all(),[
            'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);
        // guardar imagen
        if (!$image || $validate->fails()) {
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Error al subir imagen.' 
            );
        } else {
            $image_name = time().$image->getClientOriginalName();
            \Storage::disk('images')->put($image_name, \File::get($image));
            $data = array(
                'code' => 200,
                'status' => 'success',
                'image' => $image_name 
            );

        }


        return response()->json($data, $data['code']);
    }


    public function getImage($filename){
        $isset = \Storage::disk('images')->exists($filename);

        if ($isset) {
            $file = \Storage::disk('images')->get($filename);
            return new Response($file, 200);
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'La imagen no existe.' 
            );
            return response()->json($data, $data['code']);
        }
    }

	public function getHotelsByCategory($id){
		$hotels = Hotels::where('category_id', $id)->get();

		return response()->json([
			'status' => 'success',
			'hotels' => $hotels
		], 200);
	}    

	public function getHotelsByUser($id){
		$hotels = Hotels::where('user_id', $id)->get();

		return response()->json([
			'status' => 'success',
			'hotels' => $hotels
		], 200);
	}


}

