<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Category;

class CategoryController extends Controller
{
   	public function __contruct(){
    	$this->middleware('api.auth', ['except'=>['index','show']]);
    }

    public function index(){
    	$category = Category::all();

    	return response()->json([
			'code' => 200,
			'status' => 'success',
    		'category' => $category
    	]);
    }

 	public function show($id){
    	$category = Category::find($id);

    	if (is_object($category)) {
    		$data = [
				'code' => 200,
				'status' => 'success',
	    		'category' => $category
	    	];
    	} else {
    		$data = [
				'code' => 404,
				'status' => 'error',
	    		'message' => 'No se encontró la categoria'
	    	];
    	}

    	return response()->json($data, $data['code']);
    }

    public function store(Request $request){
    	// Recoger los datos por post
    	$json = $request->input('json', null);
    	$params_array = json_decode($json, true);

    	// Validar los datos
    	$validate = \Validator::make($params_array,[
    		'name' => 'required'
    	]);
       	// Guardar la categoria
    	if ($validate->fails()) {
    		$data = [
				'code'    => 404,
				'status'  => 'error',
				'message' => 'No se ha guardado la categoria'
	    	];
    	} else {
    		$category = new Category();
			$category->name        = $params_array['name'];
			$category->save();
			$data = array(
				'code'   => 200,
				'status' => 'success',
				'category'   => $category
			);
    	}
    	// devolver respuesta
    	return response()->json($data, $data['code']);
    }

    public function update($id, Request $request){
    	$json = $request->input('json', null);
    	$params_array = json_decode($json, true);

    	if (!empty($params_array)) {
    		// Validar los datos
	    	$validate = \Validator::make($params_array,[
	    		'name' => 'required'
	    	]);
	    	unset($params_array['id']);
	    	unset($params_array['created_at']);

	    	$category_update = Category::where('id', $id)->update($params_array);
			$data = array(
				'code'   => 200,
				'status' => 'success',
				'category'   => $params_array
			);

    	} else {
    		$data = [
				'code'    => 404,
				'status'  => 'error',
				'message' => 'No has enviado ninguna categoria.'
	    	];
    	}
    	// devolver respuesta
    	return response()->json($data, $data['code']);
    }

    public function destroy($id, Request $request){
    	$json = $request->input('json', null);
    	$params_array = json_decode($json, true);

    	$category = Category::find($id);
        if (is_object($category) && !empty($category)) {
        	$category->delete();
			$data = array(
				'code'   => 200,
				'status' => 'success',
				'message'   => 'La categoria ha sido eliminada'
			);
        } else {
    		$data = [
				'code'    => 404,
				'status'  => 'error',
				'message' => 'No has enviado ninguna categoria.'
	    	];
        }

    	// devolver respuesta
    	return response()->json($data, $data['code']);
    }



}
