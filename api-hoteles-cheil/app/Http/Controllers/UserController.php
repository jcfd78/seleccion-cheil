<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

class UserController extends Controller
{
    public function index(Request $request){
        $user = User::all();

        return response()->json([
            'code' => 200,
            'status' => 'success',
            'user' => $user
        ]);
    }

    public function register(Request $request){
		$json   = $request->input('json', null);
		$params = json_decode($json);
        $params_array = json_decode($json, true);
        
        // Limpiamos los datos de espacios al inicio y fin
        if (!empty($params) && !empty($params_array)) {
            # Si no esta vacio el arreglo
            $params_array = array_map('trim', $params_array);
            // Validamos los datos enviados
            $validate = \Validator::make($params_array,[
                'name'     => 'required|alpha',
                'surname'  => 'required|alpha',
                'email'    => 'required|email|unique:users',
                'password' => 'required'
            ]);

            if ($validate->fails()){
                // la validacion ha fallado
                $data = array(
                    'status'  => 'error',
                    'code'    => 404,
                    'message' => 'El usuario no se ha creado',
                    'errors'  => $validate->errors() 
                );
            } else {
                // la validacion ha pasado correctamente

                // ciframos la contraseña
                $pwd = hash('sha256', $params->password);
                // Creamos el usuario
                $user           = new User();
                $user->name     = $params_array['name'];
                $user->surname  = $params_array['surname'];
                $user->email    = $params_array['email'];
                $user->password = $pwd;
                $user->role 	= 'user';
                $user->save();
                // se crea la respuesta
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'El usuario se ha creado correctamente',
                    'errors' => null,
                    'user' => $user
                );
            }
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos',
            );
        }
        return response()->json($data, $data['code']);
    } 
    // Termina public function register *******************************************

    public function login(Request $request){
        $jwtAuth  = new \JwtAuth();
        //recibimos los datos por post
        $json         = $request->input('json', null);
        $params       = json_decode($json);
        $params_array = json_decode($json, true);

        // Validamos los datos enviados
        $validate = \Validator::make($params_array,[
            'email'    => 'required|email',
            'password' => 'required'
        ]);

        if ($validate->fails()){
            // la validacion ha fallado
            $signup = array(
                'status'  => 'error',
                'code'    => 404,
                'message' => 'El usuario no se podido validar.',
                'errors'  => $validate->errors() 
            );
        } else {
            // ciframos la contraseña
            $pwd = hash('sha256', $params->password);
            //devolvemos token o datos
            $signup = $jwtAuth->signup($params->email, $pwd);
            if (!empty($params->gettoken)) {
                $signup = $jwtAuth->signup($params->email, $pwd, true);
            }
        }

        return response()->json($signup, 200);
    }
    // termina public function login ***************************************************

    public function update(Request $request){
        // Comprobamos que el usuario esta identificado
        $token = $request->header('Authorization');
        $jwtAuth = new \JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);
        
        // recogemos los datos por post
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);

        if ($checkToken && !empty($params_array)) {
            //Actualizamos el usuario
            // Tomamos los datos del usuario identificado
            $user = $jwtAuth->checkToken($token, true);

            // validamos los datos
            $validate = \Validator::make($params_array,[
                'name'     => 'required|alpha',
                'surname'  => 'required|alpha',
                'email'    => 'required|email|unique:users,'.$user->sub
            ]);
            // quitamos los datos no actualizables
            unset($params_array['id']);
            unset($params_array['role']);
            unset($params_array['created_at']);
            unset($params_array['password']);
            //Actualizar usuario en BD
            $user_update = User::where('id', $user->sub)->update($params_array);
            // Devolvemos array con resultado
            $data = array(
                'code' => 200,
                'status' => 'success',
                'user' => $user,
                'changes' => $params_array
            );
        } else {
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'El usuario no está identificado.' 
            );
        }

        return response()->json($data, $data['code']);
    }
    // Termina public function update *****************************************

    public function upload(Request $request){
        // recoger los datos de la peticion
        $image = $request->file('file0');
        // Validamos la imagen recibida
        $validate = \Validator::make($request->all(),[
            'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);
        // guardar imagen
        if (!$image || $validate->fails()) {
            $data = array(
                'code' => 400,
                'status' => 'error',
                'message' => 'Error al subir imagen.' 
            );
        } else {
            $image_name = time().$image->getClientOriginalName();
            \Storage::disk('users')->put($image_name, \File::get($image));
            $data = array(
                'code' => 200,
                'status' => 'success',
                'image' => $image_name 
            );

        }


        return response()->json($data, $data['code']);
    }

    public function getImage($filename){
        $isset = \Storage::disk('users')->exists($filename);

        if ($isset) {
            $file = \Storage::disk('users')->get($filename);
            return new Response($file, 200);
        } else {
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'La imagen no existe.' 
            );
            return response()->json($data, $data['code']);
        }
    }

    public function detail($id){
        $user = User::find($id);

        if (is_object($user)) {
            $data = array(
                'code' => 200,
                'status' => 'success',
                'user' => $user 
            );
        } else {
             $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'El usuario no existe.' 
            );
        }
         return response()->json($data, $data['code']);
    }


}
