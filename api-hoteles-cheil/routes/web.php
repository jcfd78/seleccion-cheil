<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Cargamos clases
use App\Http\Middleware\ApiAuthMiddleware; 

	// Rutas de controlador de usuarios
	Route::post('/api/register', 'UserController@register');
	Route::post('/api/login', 'UserController@login');
	Route::put('/api/user/update', 'UserController@update')->middleware(ApiAuthMiddleware::class); // Con este middelware validamos que el usuario este registrado;
	Route::post('/api/user/upload', 'UserController@upload')->middleware(ApiAuthMiddleware::class); // Con este middelware validamos que el usuario este registrado;
	Route::get('/api/user/avatar/{filename}', 'UserController@getImage');
	Route::get('/api/user/detail/{id}', 'UserController@detail');
	Route::get('/api/user', 'UserController@index');

	//Rutas de category
	Route::get('api/category', 'CategoryController@index');
	Route::get('api/category/{id}', 'CategoryController@show');
	Route::post('api/category', 'CategoryController@store')->middleware(ApiAuthMiddleware::class); // Con este middelware validamos que el usuario este registrado
	Route::put('api/category/{id}', 'CategoryController@update')->middleware(ApiAuthMiddleware::class); // Con este middelware validamos que el usuario este registrado
	Route::delete('api/category/{id}', 'CategoryController@destroy')->middleware(ApiAuthMiddleware::class); // Con este middelware validamos que el usuario este registrado

	// Rutas Hoteles
	Route::get('api/hotels', 'HotelsController@index');
	Route::get('api/hotels/{id}', 'HotelsController@show');
	Route::post('api/hotels', 'HotelsController@store')->middleware(ApiAuthMiddleware::class); // Con este middelware validamos que el usuario este registrado
	Route::put('api/hotels/{id}', 'HotelsController@update')->middleware(ApiAuthMiddleware::class); // Con este middelware validamos que el usuario este registrado
	Route::delete('api/hotels/{id}', 'HotelsController@destroy')->middleware(ApiAuthMiddleware::class); // Con este middelware validamos que el usuario este registrado
	Route::post('/api/hotels/upload', 'HotelsController@upload')->middleware(ApiAuthMiddleware::class); // Con este middelware validamos que el usuario este registrado;
	Route::get('/api/hotels/category/{id}', 'HotelsController@getHotelsByCategory');
	Route::get('/api/hotels/user/{id}', 'HotelsController@getHotelsByUser');