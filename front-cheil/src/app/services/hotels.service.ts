import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Hotels} from '../models/hotels';
import {global} from './global';

@Injectable()
export class HotelsService{
	public url: string;
	

	constructor(
		public _http: HttpClient	
	){
		this.url = global.url;
	}

	create(token, hotels):Observable<any>{
		console.log(hotels);
		let json = JSON.stringify(hotels);
		let params = 'json='+json;
		let headers =  new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
										.set('Authorization', token);

		return this._http.post(this.url+'hotels', params, {headers:headers});
	}

	getHotels():Observable<any>{
		let headers =  new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+'hotels', {headers:headers}); 
	}

	getHotel(id):Observable<any>{
		let headers =  new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

		return this._http.get(this.url+'hotels/'+id, {headers:headers}); 
	}

	update(token, hotels, id):Observable<any>{
		let json = JSON.stringify(hotels);
		let params = 'json='+json;
		let headers =  new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
										.set('Authorization', token);

		return this._http.put(this.url+'hotels/'+id, {headers:headers}); 
	}
}