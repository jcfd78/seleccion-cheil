import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { UserService } from '../../services/user.service';
import { CategoryService } from '../../services/category.service';
import { HotelsService } from '../../services/hotels.service';
import { Hotels } from '../../models/hotels';
import { global } from '../../services/global';

@Component({
	selector: 'app-hotel-edit',
	templateUrl: '../hotel-new/hotel-new.component.html',
	providers: [UserService, CategoryService, HotelsService]
})
export class HotelEditComponent implements OnInit {

	public page_title: string;
	public token;
	public identity;
	//public category: Category;
	public status;
	public categories;
	public hotels: Hotels;
	public is_edit: true;
	public froala_options: Object = {
		    charCounterCount: true,
		    toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
		    toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
		    toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
		    toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
		  };
	public afuConfig = {
	    multiple: false,
	    formatsAllowed: ".jpg,.png,.jpeg,.gif",
	    maxSize: "50",
	    uploadAPI:  {
	      url: global.url+'hotel/upload',
	      headers: {
			"Authorization" : this._userService.getToken()
	      }
	    },
	    theme: "attachPin",
	    hideProgressBar: true,
	    hideResetBtn: true,
	    hideSelectBtn: false,
	    attachPinText: 'Sube la imagen del hotel'
	};


	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _userService: UserService,
		private _categoryService: CategoryService,
		private _hotelsService: HotelsService
	) {
		this.page_title='Editar hotel';
		this.identity =  this._userService.getIdentity();
		this.token =  this._userService.getToken();
	}

	ngOnInit() {
		this.hotels = new Hotels(1,this.identity.id,1,"","",null, null ,null, "");
		this.getCategories();
		this.getHotel();
	}

	getCategories(){
		this._categoryService.getCategories().subscribe(
			response => {
				if (response.status == 'success') {
					this.categories = response.category;
					console.log(this.categories);
				}
			},
			error => {
				console.log(error);
			}
		);
	}

	imageUpload(datos){
		let data = JSON.parse(datos.response);

		this.hotels.image = data.image;
	}

	onSubmit(form){
		this._hotelsService.create(this.token, this.hotels).subscribe(
			response => {
				if (response.status == 'success') {
					this.hotels = response.hotels;
					this.status = 'success';
					this._router.navigate(['/inicio']);
				}
			},
			error=>{
				console.log(error);
			}
		);
	}

	getHotel(){
		this._route.params.subscribe( params => {
			let id = +params['id'];

			this._hotelsService.update(this.token, this.hotels, this.hotels.id).subscribe(
				response=>{
					if (response.status == 'success') {
						this.status = 'success';
						//this.hotels = response.hotels,
						//console.log(this.hotels);
						this._router.navigate(['/hotel', this.hotels.id]);
					} else {
						this.status = 'error';
					}
				},
				error =>{
					this.status = 'error';
					console.log(error);
				}
			);


		});

	}

}
