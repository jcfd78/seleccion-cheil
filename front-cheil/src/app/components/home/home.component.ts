import { Component, OnInit } from '@angular/core';
import { Hotels } from '../../models/hotels';
import { HotelsService } from '../../services/hotels.service';
import { global } from '../../services/global';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HotelsService, UserService]
})
export class HomeComponent implements OnInit {
	public page_title: string;
	public url;
	public hotels: Array<Hotels>;
	public identity;
	public token;

	constructor(
		private _hotelsService: HotelsService,
		private _userService: UserService
	) {
		this.page_title ='Inicio';
		this.url        = global.url;
		this.identity   = this._userService.getIdentity();
		this.token      = this._userService.getToken();
	}

	ngOnInit() {
		this.getHotels();
	}

	getHotels(){
		this._hotelsService.getHotels().subscribe(
			response=>{
				if (response.status == 'success') {
					this.hotels = response.hotels,
					console.log(this.hotels);
				}
			},
			error =>{
				console.log(error);
			}
		);
	}

}
