import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Hotels } from '../../models/hotels';
import { HotelsService } from '../../services/hotels.service';
import { global } from '../../services/global';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-hotel-detail',
  templateUrl: './hotel-detail.component.html',
  styleUrls: ['./hotel-detail.component.css'],
  providers: [HotelsService, UserService]
})
export class HotelDetailComponent implements OnInit {

	public hotels: Hotels;

  	constructor(
  		private _hotelsService: HotelsService,
  		private _route: ActivatedRoute,
  		private _router: Router
  	) { }

  ngOnInit() {
  	this.getHotel();
  }

  getHotel(){
		this._route.params.subscribe( params => {
			let id = +params['id'];

			this._hotelsService.getHotel(id).subscribe(
				response=>{
					if (response.status == 'success') {
						this.hotels = response.hotels,
						console.log(this.hotels);
					} else {
						this._router.navigate(['inicio']);
					}
				},
				error =>{
					console.log(error);
				}
			);


		});

	}

}
