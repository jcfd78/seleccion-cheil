export class Hotels{
	constructor(
		public id: number,
		public user_id: number,
		public category_id: number,
		public hotel: string,
		public content: string,
		public image: string,
		public stars: number,
		public address: string,
		public createdAt: any
	){}
}