import { Component, OnInit, DoCheck } from '@angular/core';
import {UserService} from './services/user.service';
import {CategoryService} from './services/category.service';
import {global} from './services/global';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
	providers: [UserService, CategoryService]
})
export class AppComponent implements OnInit, DoCheck {
	public title = 'Hoteles Cheil';
	public identity;
	public token;
	public url;
	public categories;

	constructor(
		private _userService: UserService,
		private _categoryService: CategoryService
	){
		this.loadUser();
		this.url = global.url;
	}

	ngOnInit(){
		console.log('Valida ngoninit');
		this.getCategories();
	}

	ngDoCheck(){
		this.loadUser();
		//this.getCategories();
	}

	loadUser(){
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
	}

	getCategories(){
		this._categoryService.getCategories().subscribe(
			response => {
				//console.log('Esta es la categorias');
				//console.log(response);
				if(response.status == 'success'){
					this.categories = response.category;
				}
			},
			error => {
				console.log(error);
			}
		);
	}
}
