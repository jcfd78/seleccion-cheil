// Import necesarios
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// importar componentes
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/error/error.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { CategoryNewComponent } from './components/category-new/category-new.component';
import { HotelNewComponent } from './components/hotel-new/hotel-new.component';
import { HotelDetailComponent } from './components/hotel-detail/hotel-detail.component';
import { HotelEditComponent }from './components/hotel-edit/hotel-edit.component';

// Definicion de rutas
const appRoutes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'inicio', component: HomeComponent},
	{path: 'login', component: LoginComponent},
	{path: 'logout/:sure', component: LoginComponent},
	{path: 'registro', component: RegisterComponent},
	{path: 'ajustes', component: UserEditComponent},
	{path: 'crear-categoria', component: CategoryNewComponent},
	{path: 'crear-hotel', component: HotelNewComponent},
	{path: 'hotel/:id', component: HotelDetailComponent},
	{path: 'editar-hotel/:id', component: HotelEditComponent},
	{path: '**', component: ErrorComponent},
];


// exportar configuracion de rutas
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);