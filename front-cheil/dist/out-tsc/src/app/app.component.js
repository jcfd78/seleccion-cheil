import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { UserService } from './services/user.service';
let AppComponent = class AppComponent {
    constructor(_userService) {
        this._userService = _userService;
        this.title = 'Hoteles Cheil';
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
    }
    ngOnInit() {
        console.log('Valida ngoninit');
    }
    ngDoCheck() {
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
    }
    loadUser() {
    }
};
AppComponent = __decorate([
    Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css'],
        providers: [UserService]
    })
], AppComponent);
export { AppComponent };
//# sourceMappingURL=app.component.js.map