export class Hotels {
    constructor(id, user_id, category_id, hotel, content, image, stars, address, createdAt) {
        this.id = id;
        this.user_id = user_id;
        this.category_id = category_id;
        this.hotel = hotel;
        this.content = content;
        this.image = image;
        this.stars = stars;
        this.address = address;
        this.createdAt = createdAt;
    }
}
//# sourceMappingURL=hotels.js.map