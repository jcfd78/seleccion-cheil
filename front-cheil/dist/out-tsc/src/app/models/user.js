export class User {
    constructor(id, name, surname, role, email, password, description, image) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.role = role;
        this.email = email;
        this.password = password;
        this.description = description;
        this.image = image;
    }
}
//# sourceMappingURL=user.js.map