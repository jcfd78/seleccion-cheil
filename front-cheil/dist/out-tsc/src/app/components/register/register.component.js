import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
let RegisterComponent = class RegisterComponent {
    constructor(_userService) {
        this._userService = _userService;
        this.page_title = 'Registrate';
        this.user = new User(1, '', '', 'ROLE_USER', '', '', '', '');
    }
    ngOnInit() {
        console.log('Componente lanzado!!');
        console.log(this._userService.test());
    }
    onSubmit(form) {
        this._userService.register(this.user).subscribe(response => {
            if (response.status == "success") {
                this.status = response.status;
                form.reset();
            }
            else {
                this.status = 'error';
            }
        }, error => {
            this.status = 'error';
            console.log(error);
        });
    }
};
RegisterComponent = __decorate([
    Component({
        selector: 'register',
        templateUrl: './register.component.html',
        styleUrls: ['./register.component.css'],
        providers: [UserService]
    })
], RegisterComponent);
export { RegisterComponent };
//# sourceMappingURL=register.component.js.map