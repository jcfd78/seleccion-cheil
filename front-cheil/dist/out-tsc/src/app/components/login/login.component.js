import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
let LoginComponent = class LoginComponent {
    constructor(_userService, _router, _route) {
        this._userService = _userService;
        this._router = _router;
        this._route = _route;
        this.page_title = 'Identificate';
        this.user = new User(1, '', '', 'ROLE_USER', '', '', '', '');
    }
    ngOnInit() {
        // Esto se ejecuta siempre y solo cuando llega el parametro sure se cierra la sesion
        this.logout();
    }
    onSubmit(form) {
        this._userService.signup(this.user).subscribe(response => {
            if (response.status != "error") {
                this.status = 'success';
                this.token = response;
                // Objeto usuario
                this._userService.signup(this.user, true).subscribe(response => {
                    this.identity = response;
                    // Persistimos los datos del usuario y su token
                    console.log(this.token);
                    console.log(this.identity);
                    localStorage.setItem('token', this.token);
                    localStorage.setItem('identity', JSON.stringify(this.identity));
                }, error => {
                    this.status = 'error';
                    console.log(error);
                });
            }
            else {
                this.status = 'error';
            }
        }, error => {
            this.status = 'error';
            console.log(error);
        });
    }
    logout() {
        this._route.params.subscribe(params => {
            let logout = +params['sure'];
            if (logout == 1) {
                localStorage.removeItem('identity');
                localStorage.removeItem('token');
                this.identity = null;
                this.token = null;
                this._router.navigate(['inicio']);
            }
        });
    }
};
LoginComponent = __decorate([
    Component({
        selector: 'login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.css'],
        providers: [UserService]
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map