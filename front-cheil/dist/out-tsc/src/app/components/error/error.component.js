import { __decorate } from "tslib";
import { Component } from '@angular/core';
let ErrorComponent = class ErrorComponent {
    constructor() {
        this.page_title = 'Pagina no encontrada - Error 404';
    }
    ngOnInit() {
    }
};
ErrorComponent = __decorate([
    Component({
        selector: 'app-error',
        templateUrl: './error.component.html',
        styleUrls: ['./error.component.css']
    })
], ErrorComponent);
export { ErrorComponent };
//# sourceMappingURL=error.component.js.map